*** Settings ***
Library  SeleniumLibrary

*** Variables ***
${url}  https://admin-demo.nopcommerce.com/login
${browser}  chrome

*** Keywords ***
Open my Browser
    open browser    ${url}  ${browser}
    maximize browser window
Close Browsers
    Close all browsers

Open Login Page
    go to   ${url}

Input username
    [Arguments]     ${username}
    input text  id:Email    ${username}
Input password
    [Arguments]     ${password}
    input text  id:Password      ${password}
Click Login Button
    click element   xpath://button[@class="button-1 login-button"]
Click Logout Link
    click link  Logout

Error message should be visible
    page should contain     Login was unsuccessful

Dashboard page should be visibile
    page should contain     Dashboard

