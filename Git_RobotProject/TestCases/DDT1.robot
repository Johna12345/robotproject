*** Settings ***
Library  SeleniumLibrary
Resource  ../Resources/login_resources.robot
Suite Setup     Open my Browser
Suite Teardown  Close Browsers  #it will run only once after all test cases are executed
Test Template  Invalid login

#We are passing the data in this area
*** Test Cases ***                      username    password
Valid user empty password   admin@yourstore.com     ${empty}
Valid user wrong password   admin@yourstore.com     xyz
Invalid user correct password   admin@yourstore.com     admin
Invalid user empty password     admin@yourstore.com     ${empty}
Invalid user Invalid password   adn@yourstore.com   xyz

*** Keywords ***
Invalid login   #this required username and password
    [Arguments]  ${username}    ${password}     #specify the arguments
    Input username  ${username}  #from login_resources file
    Input password  ${password} #from login_resources file
    click Login Button   #from login_resources file
    Error message should be visible     #from login_resources file
