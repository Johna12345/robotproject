*** Settings ***
Library  SeleniumLibrary
Resource  ../Resources/login_resources.robot
Library  DataDriver  ../TestData/TestData.csv

Suite Setup  Open my Browser
Suite Teardown  Close Browsers
Test Template  Invalid login

*** Test Cases ***
#LoginTestWithExcel using ${username} and ${password}
LoginWithTestCaseExcel using Invalid Login ${username} ${password}   #one test case with multiple data, test case will execute multiple times from Sheet1


*** Keywords ***
Invalid login   #test template
    [Arguments]     ${username}     ${password}     #should match in xlsx file
    Input username  ${username}  #from login_resources file
    Input password  ${password} #from login_resources file
    click login button
    Error message should be visible
